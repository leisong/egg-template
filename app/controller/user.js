'use strict';

const Controller = require('egg').Controller;

/**
 * @Controller 用户管理
 */
class UserController extends Controller {
  constructor(ctx){
    super(ctx)
  }

  /**
   * @summary 创建用户
   * @description 创建用户
   * @router post /api/user
   * @request body createUserRequest *body
   * @response 200 baseResponse 创建成功
   */
  async create(){
    const {ctx,service} =this
    // 校验参数
    ctx.validate(ctx.rule.createUserRequest)
    const payload = ctx.request.body || {}
    const res = await service.user.create(payload)
    ctx.helper.success({ctx,res})
  }

  /**
   * @summary 获取用户列表
   * @description 获取用户列表
   * @router get /api/all
   * @request body baseRequest *body
   * @response 200 baseResponse 创建成功
   */
  async all(){
    const {ctx} =this
    console.log(ctx.state.user)
    const res = await ctx.model.User.findAll();
    ctx.helper.success({ctx,res})
  }
}

module.exports = UserController;
