/**
 * 用户模型 服务层
 */
'use strict';
const Service = require('egg').Service;

class UserService extends Service {
  /**
   * @param {*} payload
   */
  async create(payload) {

    const {ctx} = this
    payload.password = await this.ctx.genHash(payload.password)
    console.log(payload)
    return await this.ctx.model.User.create(payload);
  }
  async finUserById(id){
    const {ctx} = this
    return await this.ctx.model.User.findByPk(id);
  }
}

module.exports = UserService;
