const Service = require('egg').Service
/**
 * 登录 登出 服务层
 */
class UserAccessService extends Service{
    async login(payload){
        const {ctx,service} = this
        const user = await service.user.finUserById(payload.id)
        if(!user){
            ctx.throw(404,'user not found')
        }
        let verifyPsw = await ctx.compare(payload.password,user.password)

        if(!verifyPsw){
            ctx.throw(404,'user password is wrrong')
        }
        return {
            token:await service.actionToken.apply(user.id)
        }
    }
    async logout(){}

    async current(){
        const {ctx,service} = this
        // ctx.state.user可以提取到jwt编码的data
        const id = ctx.state.user.data.user
        const user = await this.ctx.model.User.findById(id)
        if(!user){
            ctx.throw(404,'user is not found')
        }
        user.password = 'How old are you?'
        return user
    }
}

module.exports = UserAccessService