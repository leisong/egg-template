/**
 * 
 * @description User数据模型  
 */
module.exports = app =>{
    const {STRING,INTEGER} = app.Sequelize;

    const User = app.model.define('user',
        { 
            id: { type: INTEGER, primaryKey: true, autoIncrement: true },
            mobile: STRING(30),
            realName: STRING(30),
            password: STRING(255) 
        },
    );
    // 开启后格式化数据库
    // User.sync({force:true})
    return User;
}