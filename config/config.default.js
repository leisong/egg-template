/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1606198609795_7866';

  // add your middleware config here
  config.middleware = ['errorHandler'];

  config.onerror = {
    json(err, ctx) {
      // json hander
      ctx.body = { code:ctx.status,message: 'error' };
    },
    html(err, ctx) {
      // html hander
      ctx.body = '<h3>'+err+'</h3>';
      ctx.status = 500;
    },
  }

  config.jwt = {
    secret: 'Great4-M',
    enable:true,
    match:/^\/api/
  }

  config.swaggerdoc = {
    dirScanner: './app/controller',
    appInfo:{
      title:'接口文档',
      description:'接口描述',
      version:'1.0.0'
    },
    schemes:['http','https'],
    consumes:['application/json'],
    produces:['application/json'],
    enableSecurity:false,
    // enableValidate:true,
    routerMap:true,
    enable:true
  }


  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
    sequelize: {
      dialect: 'mysql',
      host: '127.0.0.1',
      port: '3306',
      username: 'root',
      database: 'demo',
      password:'songlei01'
    }
  };

  

  return {
    ...config,
    ...userConfig,
  };
};
