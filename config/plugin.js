'use strict';

/** @type Egg.EggPlugin */
module.exports = {
  // had enabled by egg
  // static: {
  //   enable: true,
  // }
  // 使用sequelize mysql
  sequelize: {
    enable: true,
    package: 'egg-sequelize',
  },
  // 接口可视化
  swagger:{
    enable:true,
    package:'egg-swagger-doc-feat'
  },
  // 参数验证
  validate:{
    enable:true,
    package:'egg-validate'
  },
  // 加密插件
  bcrypt:{
    enable:true,
    package:'egg-bcrypt'
  },
  jwt:{
    enable:true,
    package:'egg-jwt'
  }
};
